/* eslint-disable prettier/prettier */
import { config as dotenvConfig } from 'dotenv';
import { TypeOrmModuleOptions } from '@nestjs/typeorm/dist/interfaces/typeorm-options.interface';
import * as path from 'path';
import { DataSource, DataSourceOptions } from 'typeorm';

dotenvConfig({ path: '.env' });

export const databaseConfig: TypeOrmModuleOptions  = {
  type: 'postgres',
  host: `${process.env['POSTGRES_HOST']}`,
  port: +`${process.env['POSTGRES_PORT']}`,
  username: `${process.env['POSTGRES_USER']}`,
  password: `${process.env['POSTGRES_PASSWORD']}`,
  database: `${process.env['POSTGRES_DATABASE']}`,
  synchronize: process.env['NODE_ENV'] !== 'PRODUCTION',

};
export const dataSource = new DataSource(databaseConfig as DataSourceOptions);
