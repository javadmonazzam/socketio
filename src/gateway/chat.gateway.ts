import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  ConnectedSocket,
} from '@nestjs/websockets';
import { Logger, OnModuleInit } from '@nestjs/common';
import {
  ServerToClientEvents,
  ClientToServerEvents,
  Message,
  User,
  Private,
} from './chat.interface';
import { Server, Socket } from 'socket.io';
import { ChatService } from './chat.service';

@WebSocketGateway({ namespace: 'chat' })
export class Chat
  implements OnGatewayConnection, OnGatewayDisconnect, OnModuleInit
{
  constructor(private gatewayService: ChatService) {}

  @WebSocketServer() server: Server = new Server<
    ServerToClientEvents,
    ClientToServerEvents
  >();
  onModuleInit() {
    this.server.on('connection', (socket) => {
      console.log(socket.id);
      console.log('connected');
    });
  }
  private logger = new Logger('');
  // event name______|||||
  @SubscribeMessage('private')
  async handleEvents(
    @ConnectedSocket() client: Socket,
    @MessageBody()
    payload: Private,
  ): Promise<Private> {
    // this.logger.log(payload.user);
    console.log(payload);
    client.emit('onMessage', {
      msg: payload,
    }); // broadcast messages
    return payload;
  }
  @SubscribeMessage('chat1')
  async handleChatEvent(
    @ConnectedSocket() client: Socket,
    @MessageBody() payload: Message,
  ): Promise<Message> {
    this.logger.log(payload);
    client.to('Room').emit('onChat', payload); // broadcast messages
    return payload;
  }

  @SubscribeMessage('get_rooms')
  getRooms() {
    return this.gatewayService.getRooms();
  }

  @SubscribeMessage('join_room')
  async handleSetClientDataEvent(
    @MessageBody()
    payload: {
      roomName: string;
      user: User;
    },
    @ConnectedSocket() client: Socket,
  ): Promise<void> {
    console.log('object<<<<<<<<<<<<<');
    client.emit('onJoin', {
      msg: `signup${payload.user}`,
    });
    client.join('Room');
    console.log(payload.user);
    if (payload.user) {
      this.logger.log(`${payload.user} is joining ${payload.roomName}`);
      await this.server.in(payload.roomName).socketsJoin(payload.roomName);
      await this.gatewayService.addUserToRoom(payload.roomName, payload.user);
    }
  }
  async handleConnection(socket: Socket): Promise<void> {
    this.logger.log(`Socket connected: ${socket.id}`);
  }
  async handleDisconnect(socket: Socket): Promise<void> {
    await this.gatewayService.removeUserFromAllRooms(socket.id);
    this.logger.log(`Socket disconnected: ${socket.id}`);
  }
}
