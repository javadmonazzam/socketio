import { IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty({})
  id: number;
  name: string;
  username: string;
}
