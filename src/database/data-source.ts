/* eslint-disable prettier/prettier */
import { DataSource, DataSourceOptions } from 'typeorm';
import { allConfig as Config } from '../../config/config';
export const dataSourceOptions: DataSourceOptions = {
  type: 'mysql',
  host: Config.database.db_host,
  port: +Config.database.db_port,
  username: Config.database.db_username,
  password: Config.database.db_password,
  database: Config.database.db_name,
  synchronize: true,
  entities: [__dirname + '/../**/*.entity.{js,ts}'],
  // migrations: ['dist/db/migrations/*js'],
};
const dataSource = new DataSource(dataSourceOptions);
export default dataSource;
