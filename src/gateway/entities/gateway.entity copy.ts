import { Column, PrimaryGeneratedColumn } from 'typeorm';
export class Gateway {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ type: 'text', nullable: true })
  user: string;
  @Column({ type: 'text', nullable: true })
  userName: string;
  @Column({ type: 'text', nullable: true })
  socketId: string;
}
