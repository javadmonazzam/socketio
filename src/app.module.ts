import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GatewayModule } from './gateway/chat.module';
import { DatabaseModule } from './database/database.module';
@Module({
  imports: [GatewayModule, DatabaseModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
