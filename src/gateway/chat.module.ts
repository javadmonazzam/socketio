import { Module } from '@nestjs/common';
import { ChatService } from './chat.service';
import { Chat } from './chat.gateway';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [Chat, ChatService],
})
export class GatewayModule {}
