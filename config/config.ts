/* eslint-disable prettier/prettier */
import { config } from 'dotenv';
import { resolve } from 'path';
// path directory
const envFilePath_all = resolve(__dirname, '..', '..', 'config', '.env');
const envFilePath_seeder = resolve(__dirname, '..', 'config', '.env');

let env = config({ path: envFilePath_all });

if (env.error) {
  env = config({ path: envFilePath_seeder });
  if (env.error) throw env.error;
}

export const allConfig = {
  database: {
    db_host: process.env.DB_HOST,
    db_port: process.env.DBPORT,
    db_username: process.env.SQL_USERNAME,
    db_password: process.env.SQL_PASS,
    db_name: process.env.DB_NAME,
  },
  setting: {
    // port 3306 ||1111
    port: process.env.PORT || 1111,
  },
  URL:{
    base_url: process.env.BASE_URL,
    base_urletc : process.env.BASE_URLETC
  },
  JWT:{
    jwt_secret: process.env.JWT_SECRET
  },
  ADMIN:{
    USERNAME: process.env.ADMIN_USERNAME,
    PASSWORD : process.env.ADMIN_PASSWORD
  },
}