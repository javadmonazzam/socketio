/* eslint-disable prettier/prettier */
import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { Room } from './chat.interface';
import { CreateUserDto } from './dto/create-user.dto';
import { ChatService } from './chat.service';
 
@Controller()
export class ChatController {
  constructor(private userService: ChatService) {}
  @Post('login')
  async login(@Body() body: CreateUserDto) {
    return await this.userService.login(body); 
  }
  @Get('api/rooms')
  async getAllRooms(): Promise<Room[]> {
    return await this.userService.getRooms();
  }

  @Get('api/rooms/:room')
  async getRoom(@Param() params): Promise<Room> {
    const rooms = await this.userService.getRooms();
    const room = await this.userService.getRoomByName(params.room);
    return rooms[room];
  }
}
