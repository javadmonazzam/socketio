/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: `${process.env['POSTGRES_HOST']}`,
      port: +`${process.env['POSTGRES_PORT']}`,
      username: `${process.env['POSTGRES_USER']}`,
      password: `${process.env['POSTGRES_PASSWORD']}`,
      database: `${process.env['POSTGRES_DATABASE']}`,
      entities: [],
      synchronize: true,
    }),
  ],
})
export class AppModule {}